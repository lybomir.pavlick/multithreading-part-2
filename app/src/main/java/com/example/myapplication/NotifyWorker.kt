package com.example.myapplication

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.*
import java.lang.Exception

class NotifyWorker(private val context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {

    override suspend fun doWork(): Result {
        return try {
            val context = applicationContext
            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)
            val notification = createNotification(
                title = "Awesome notification",
                content = "This is just a simple content",
                pendingIntent = pendingIntent
            )

            startNotifying(1000L, notification)

            Result.success()
        } catch (e: Exception) {
            Log.e(TAG, "Error showing notifications ", e)
            Result.failure()
        }
    }

    private suspend fun startNotifying(timeInterval: Long, notification: Notification): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            while (isActive) {
                val sharedPref = context.getSharedPreferences("SHOW_NOTIFICATIONS", Context.MODE_PRIVATE)
                val flag = sharedPref.getBoolean(MainActivity.SHOW_NOTIFICATION_KEY, false)
                if (flag) {
                    sendNotification(notification)
                    delay(timeInterval)
                }
            }
        }
    }

    private fun createNotification(title: String, content: String, pendingIntent: PendingIntent): Notification =
        NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_stat_name)
            .setContentTitle(title)
            .setContentText(content)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
            .build()


    private fun sendNotification(notification: Notification) {
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(++NOTIFICATION_ID, notification)
    }

    companion object {
        const val CHANNEL_ID = "CHANNEL_ID"
        var NOTIFICATION_ID = 421

        const val TAG = "NotifyWorker"
    }
}