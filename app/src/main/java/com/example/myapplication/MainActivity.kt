package com.example.myapplication

import android.app.*
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.myapplication.databinding.ActivityMainBinding
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val workManager: WorkManager = WorkManager.getInstance(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Create a notification channel
        createNotificationChannel()

        with(binding) {
            startButton.setOnClickListener{ onStartHandler() }
            stopButton.setOnClickListener{ onStopHandler() }
        }
    }

    private fun onStartHandler() {
        val sharedPref = getSharedPreferences("SHOW_NOTIFICATIONS", Context.MODE_PRIVATE) ?: return
        sharedPref.edit().apply {
            putBoolean(SHOW_NOTIFICATION_KEY, true)
            apply()
        }

        val request = PeriodicWorkRequestBuilder<NotifyWorker>(15, TimeUnit.MINUTES)
            .build()

        workManager.enqueue(request)
    }

    private fun onStopHandler() {
        val sharedPreferences = getSharedPreferences("SHOW_NOTIFICATIONS", Context.MODE_PRIVATE) ?: return
        sharedPreferences.edit().apply {
            putBoolean(SHOW_NOTIFICATION_KEY, false)
            apply()
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT).apply {
                lightColor = Color.RED
                enableLights(true)
            }
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }

    companion object {
        const val CHANNEL_ID = "CHANNEL_ID"
        const val CHANNEL_NAME = "CHANNEL_NAME"

        const val SHOW_NOTIFICATION_KEY = "isShowNotification"
    }

}